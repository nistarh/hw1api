package usersAPI;
import DTO.User;
import DTO.UserOut;
import com.sun.tools.xjc.reader.xmlschema.bindinfo.BIConversion;
import dataGenerator.UserGenerator;
import io.restassured.internal.common.assertion.Assertion;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;
import services.UserApi;

import java.nio.file.attribute.UserPrincipalLookupService;
import java.util.ArrayList;

import static org.hamcrest.Matchers.*;

public class usersApiTest {
    private UserApi userApi = new UserApi();
    @Test
    public void createValidUserTest(){
        User user = UserGenerator.getUser();
        Response response = userApi.createOneUser(user);

        UserOut actual = response.then()
                .log().all()
                .assertThat()
                .statusCode(200)
                .and()
                .time(lessThan(3000L))
                .extract()
                .body()
                .as(UserOut.class);
        UserOut expected = UserOut.builder()
                .code(200L)
                .message(user.getId().toString())
                .type("unknown")
                .build();
        Assertions.assertEquals(expected,actual);
    }
    @Test
    public void createArrayUsersTest(){
        User[] users = {UserGenerator.getUser(), UserGenerator.getUser()};

        Response response = userApi.createArrayUser(users);

        UserOut actual = response.then()
                .log().all()
                .assertThat()
                .statusCode(200)
                .and()
                .time(lessThan(3000L))
                .extract()
                .body()
                .as(UserOut.class);
        UserOut expected = UserOut.builder()
                .code(200L)
                .type("unknown")
                .message("ok")
                .build();
        Assertions.assertEquals(expected,actual);
    }
    @ParameterizedTest
    @ValueSource(strings = {"USERNAME164","USERNAME345"})
    public void getUserTestneg(String user){
        Response response = userApi.getUserByName(user);
        UserOut actual = response.then()
                .log().all()
                .statusCode(404)
                .time(lessThan(3000L))
                .extract()
                .body()
                .as(UserOut.class);

            UserOut expected = UserOut.builder()
                    .code(1L)
                    .message("User not found")
                    .type("error")
                    .build();

            Assertions.assertEquals(expected, actual);

        }
    @ParameterizedTest
    @CsvSource({"USERNAME3, 3", "USERNAME4, 4"})
    public void getUserTestpos(String user, long id){
        Response response = userApi.getUserByName(user);
        User actual = response.then()
                .log().all()
                .statusCode(200)
                .time(lessThan(3000L))
                .extract()
                .body()
                .as(User.class);
            User expected = User.builder()
                .id(id)
                .username(user)
                .firstName("FIRST NAME")
                .lastName("LAST NAME")
                .email("EMAIL")
                .password("PASSWORD")
                .phone("+7(999)-888-88-88")
                .userStatus(1L)
                .build();
        Assertions.assertEquals(expected,actual);


    }
    @Test
    public void updateUserTest(){
        User user = UserGenerator.getUser();
        if (userApi.getUserByName(user.getUsername()).statusCode() != 200)
            userApi.LogIn(user.getUsername(), user.getPassword());

        Response responce = userApi.updateUserByName(user.getUsername(), user);
        UserOut actual = responce.then()
                .log().all()
                .assertThat()
                .statusCode(200)
                .time(lessThan(3000L))
                .extract()
                .body()
                .as(UserOut.class);
        UserOut expected = UserOut.builder()
                .code(200L)
                .type("unknown")
                .message("3")
                .build();
        Assertions.assertEquals(expected, actual);


    }
    @Test
    public void LogInTest (){
        User user = UserGenerator.getUser();

        Response response = userApi.LogIn(user.getUsername(), user.getPassword());

        UserOut actual = response.then()
                .log().all()
                .assertThat()
                .statusCode(200)
                .and()
                .extract()
                .body()
                .as(UserOut.class);

        UserOut expected = UserOut.builder()
                .code(200L)
                .message("logged in user session")
                .type("unknown")
                .build();
        Assertions.assertEquals(expected.getCode(), actual.getCode());
        Assertions.assertEquals(expected.getType(), actual.getType());
        Assertions.assertTrue(actual.getMessage().contains(expected.getMessage()));

    }
    @Test
    public void LogOutTest () {
        Response response = userApi.LogOut();
        UserOut actual = response.then()
                .log().all()
                .assertThat()
                .statusCode(200)
                .and()
                .extract()
                .body()
                .as(UserOut.class);

        UserOut expected = UserOut.builder()
                .code(200L)
                .message("ok")
                .type("unknown")
                .build();
        Assertions.assertEquals(expected, actual);
    }
    @Test
    public void deleteUserTest() {

        User user = UserGenerator.getUser();
        userApi.createOneUser(user);

        Response response = userApi.DeleteUser(user.getUsername());
        UserOut actual = response.then()
                .log().all()
                .assertThat()
                .statusCode(200)
                .and()
                .extract()
                .body()
                .as(UserOut.class);

        UserOut expected = UserOut.builder()
                .code(200L)
                .message(user.getUsername())
                .type("unknown")
                .build();
        Assertions.assertEquals(expected, actual);
    }
    @Test
    public void createListUsersTest() {

        ArrayList<User> users = new ArrayList<>();
        for (int i = 1; i < 3; i++) {
            users.add(UserGenerator.getUser());
        }
        Response response = userApi.createListUsers(users);

        UserOut actual = response.then()
                .log().all()
                .assertThat()
                .statusCode(200)
                .and()
                .extract()
                .body()
                .as(UserOut.class);

        UserOut expected = UserOut.builder()
                .code(200L)
                .message("ok")
                .type("unknown")
                .build();

        Assertions.assertEquals(expected, actual);
    }
}



