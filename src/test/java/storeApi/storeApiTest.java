package storeApi;

import DTO.*;
import dataGenerator.OrderGenerator;
import io.restassured.module.jsv.JsonSchemaValidator;
import io.restassured.response.Response;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import services.StoreApi;


public class storeApiTest {
    @Test
    public void placeOrderTest() {
        Order order = OrderGenerator.getNewOrder();
        StoreApi storeService = new StoreApi();
        Response response = storeService.PlaceOrder(order);


        OrderOut actual = response.then()
                .log().all()
                .assertThat()
                .statusCode(200)
                .and()
                .extract()
                .body()
                .as(OrderOut.class);

        OrderOut expected = OrderOut.builder()
                .id(7L)
                .petId(0L)
                .quantity(4L)
                .shipDate("2022-05-10T10:04:03.580+0000")
                .status("placed")
                .complete(true)
                .build();

        Assertions.assertEquals(expected, actual);
    }


    @Test
    public void findOrderTest() {
        Order order = OrderGenerator.getNewOrder();
        StoreApi storeService = new StoreApi();
        if (storeService.FindOrder(order).statusCode() != 200)
            storeService.PlaceOrder(order);

        Response response = storeService.FindOrder(order);

        OrderOut actual = response.then()
                .log().all()
                .assertThat()
                .statusCode(200)
                .and()
                .extract()
                .body()
                .as(OrderOut.class);

        OrderOut expected = OrderOut.builder()
                .id(7L)
                .petId(0L)
                .quantity(4L)
                .shipDate("2022-05-10T10:04:03.580+0000")
                .status("placed")
                .complete(true)
                .build();

        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void deleteOrderTest() {

        Order order = OrderGenerator.getNewOrder();
        StoreApi storeService = new StoreApi();
        if (storeService.FindOrder(order).statusCode() != 200)
            storeService.PlaceOrder(order);

        Response response = storeService.DeleteOrder(order);

        ApiResp actual = response.then()
                .log().all()
                .assertThat()
                .statusCode(200)
                .and()
                .extract()
                .body()
                .as(ApiResp.class);

        ApiResp expected = ApiResp.builder()
                .code(200)
                .message("7")
                .type("unknown")
                .build();

        Assertions.assertEquals(expected, actual);
    }
    @Test
    public void getinventoryTest() {
        StoreApi storeService = new StoreApi();
        Response response = storeService.GetInventory();
        response
                .then()
                .assertThat()
                .statusCode(200);
    }

}
