package dataGenerator;

import DTO.User;

public class UserGenerator {
    private static long id = 0;

    public static User getUser(){
        id++;
        return User.builder()
                .email("EMAIL")
                .userStatus(1L)
                .firstName("FIRST NAME")
                .id(id)
                .lastName("LAST NAME")
                .password("PASSWORD")
                .phone("+7(999)-888-88-88")
                .username("USERNAME"+id)
                .build();
    }
}
