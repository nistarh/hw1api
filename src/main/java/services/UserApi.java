package services;

import DTO.User;
import com.atlassian.oai.validator.restassured.OpenApiValidationFilter;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.util.ArrayList;

import static io.restassured.RestAssured.given;

public class UserApi {
    private final String BASE_URI = "https://petstore.swagger.io/v2";
    private final String SWAGGER_JSON = "https://petstore.swagger.io/v2/swagger.json";
    private final OpenApiValidationFilter validationFilter = new OpenApiValidationFilter(SWAGGER_JSON);
    private RequestSpecification spec;
    private RequestSpecification specgetuser;

    public UserApi() {
        spec = given()
                .log().all()
                .baseUri(BASE_URI)
                /*.filter(validationFilter)*/
                .contentType(ContentType.JSON);
        specgetuser = given()
                .log().all()
                .baseUri(BASE_URI)
                .filter(validationFilter);
    }

    public Response createOneUser(User user) {
        return given(spec)
                .body(user)
                .when()
                .post("/user");

    }

    public Response createArrayUser(User[] users) {
        return given(spec)
                .body(users)
                .when()
                .post("/user/createWithArray");

    }

    public Response createListUsers(ArrayList<User> users) {
        return given(spec)
                .body(users)
                .when()
                .post("/user/createWithList");
    }

    public Response getUserByName(String user) {
        return given(specgetuser)
                .when()
                .get("/user/" + user);

    }

    public Response updateUserByName(String name, User user) {
        return given(spec)
                .contentType(ContentType.JSON)
                .body(user)
                .when()
                .put("/user/" + name);
    }

    public Response LogIn(String name, String pass) {
        return given(spec)
                .queryParams("username", name)
                .queryParams("password", pass)
                .when()
                .get("/user/login");

    }

    public Response LogOut() {
        return given(spec)
                .when()
                .get("/user/logout");
    }

    public Response DeleteUser(String name) {
        return given(spec)
                .when()
                .delete("/user/" + name);

    }
}

