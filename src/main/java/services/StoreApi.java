package services;
import DTO.Order;
import com.atlassian.oai.validator.restassured.OpenApiValidationFilter;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.util.ArrayList;

import static io.restassured.RestAssured.given;
public class StoreApi {

    private final String BASE_URI = "https://petstore.swagger.io/v2";
    private final String SWAGGER_JSON = "https://petstore.swagger.io/v2/swagger.json";
    private final OpenApiValidationFilter validationFilter = new OpenApiValidationFilter(SWAGGER_JSON);
    private RequestSpecification spec;
    private RequestSpecification specgetuser;

    public StoreApi() {
        spec = given()
                .log().all()
                .baseUri(BASE_URI)
                /*.filter(validationFilter)*/
                .contentType(ContentType.JSON);
    }

    public Response PlaceOrder(Order order){
        return given(spec)
                .body(order)
                .when()
                .post("/store/order");
    }
    public Response FindOrder(Order order){
        return given(spec)
                .when()
                .get("/store/order/"+order.getId());
    }

    public Response DeleteOrder(Order order){
        return given(spec)
                .body(order)
                .when()
                .delete("/store/order/"+order.getId());
    }

    public Response GetInventory(){
        return given(spec)
                .when()
                .get("/store/inventory");
    }



}
